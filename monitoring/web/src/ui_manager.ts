export class UIManager {
    isVerticalBarDragging: Boolean = false

    navBar: HTMLElement
    mainContainer: HTMLElement
    treeContainer: HTMLElement
    verticalDivider: HTMLElement
    detailContainer: HTMLElement
    progressBarGroupContainer: HTMLElement
    treeContainerWidthPercent: number
    verticalDividerWidth: number = 10;


    constructor() {
        this.treeContainerWidthPercent = 0.5
    }

    assignElements() {
        this.navBar = document.getElementById("navbar")
        this.mainContainer = document.getElementById("main-container")
        this.treeContainer = document.getElementById("tree-container")
        this.verticalDivider = document.getElementById("vertical-divider")
        this.detailContainer = document.getElementById("detail-container")
        this.progressBarGroupContainer =
            document.getElementById("progress-bar-group")

        this.verticalDivider.addEventListener(
            'mousedown',
            this.dividerMouseDownHandler.bind(this),
        )

        this.mainContainer.addEventListener(
            'mousemove',
            this.dividerMouseMoveHandler.bind(this),
        )

        this.mainContainer.addEventListener(
            'mouseup',
            this.dividerMouseUpHandler.bind(this),
        )
    }

    dividerMouseDownHandler(e: MouseEvent) {
        // const x = e.clientX;
        // const y = e.clientY;
        // leftWidth = leftSide.getBoundingClientRect().width;

        // document.addEventListener('mousemove', mouseMoveHandler);
        // document.addEventListener('mouseup', mouseUpHandler);

        this.isVerticalBarDragging = true;
        console.log("dragging start")
    }

    dividerMouseMoveHandler(e: MouseEvent) {
        console.log("mouse move")
        if (!this.isVerticalBarDragging) {
            return
        }

        console.log("dragging")

        const dx = e.clientX;
        const windowWidth = window.innerWidth;
        const percent = (dx - this.verticalDividerWidth / 2) / windowWidth;

        this.treeContainerWidthPercent = percent

        this.resize();
    }

    dividerMouseUpHandler(e: MouseEvent) {
        this.isVerticalBarDragging = false;

        console.log("dragging end")
    }



    //     document.addEventListener('DOMContentLoaded', function() {
    //     const resizer = document.getElementById('vertical-divider');
    //     const leftSide = resizer.previousElementSibling;
    //     // const rightSide = resizer.nextElementSibling;
    //     const rightSide = document.getElementById('detail-container');

    //     let x = 0;
    //     let y = 0;
    //     let leftWidth = 0;

    //     const mouseDownHandler = function (e) {
    //         x = e.clientX;
    //         y = e.clientY;
    //         leftWidth = leftSide.getBoundingClientRect().width;

    //         document.addEventListener('mousemove', mouseMoveHandler);
    //         document.addEventListener('mouseup', mouseUpHandler);
    //     };

    //     const mouseMoveHandler = function (e) {
    //         const dx = e.clientX - x;
    //         const dy = e.clientY - y;

    //         const newLeftWidth = ((leftWidth + dx) * 100) / resizer.parentNode.getBoundingClientRect().width;
    //         leftSide.style.width = `${newLeftWidth}%`;

    //         resizer.style.cursor = 'col-resize';
    //         document.body.style.cursor = 'col-resize';

    //         leftSide.style.userSelect = 'none';
    //         leftSide.style.pointerEvents = 'none';

    //         rightSide.style.userSelect = 'none';
    //         rightSide.style.pointerEvents = 'none';
    //     };

    //     const mouseUpHandler = function () {
    //         resizer.style.removeProperty('cursor');
    //         document.body.style.removeProperty('cursor');

    //         leftSide.style.removeProperty('user-select');
    //         leftSide.style.removeProperty('pointer-events');

    //         rightSide.style.removeProperty('user-select');
    //         rightSide.style.removeProperty('pointer-events');

    //         document.removeEventListener('mousemove', mouseMoveHandler);
    //         document.removeEventListener('mouseup', mouseUpHandler);
    //     };

    //     resizer.addEventListener('mousedown', mouseDownHandler);
    // });

    resize() {
        const windowHeight = window.innerHeight;
        const windowWidth = window.innerWidth;

        const navHeight = this.navBar.offsetHeight;
        const progressBarGroupHeight =
            this.progressBarGroupContainer.offsetHeight
        const containerHeight =
            windowHeight - navHeight - progressBarGroupHeight;


        let treeWidth = windowWidth * this.treeContainerWidthPercent;
        if (treeWidth > windowWidth - this.verticalDividerWidth) {
            treeWidth = windowWidth - this.verticalDividerWidth;
        }

        const detailContainerWidth =
            windowWidth - treeWidth - this.verticalDividerWidth;

        this.mainContainer.style.height = `${containerHeight}px`
        this.mainContainer.style.width = `${windowWidth}px`
        this.treeContainer.style.height = `${containerHeight}px`;
        this.treeContainer.style.width = `${treeWidth}px`;
        this.verticalDivider.style.height = `${containerHeight}px`;
        this.verticalDivider.style.width = `${this.verticalDividerWidth}px`;
        this.detailContainer.style.height = `${containerHeight}px`;
        this.detailContainer.style.width = `${detailContainerWidth}px`;
    }
}

function doTask(task: string, done: any) { // Callback function
    console.log(task);
    done(task);
} 